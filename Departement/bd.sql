Create database test;
create role test login PASSWORD 'test';
 alter database test owner to test;
 \c test;

create sequence sqemp;
create sequence sqdpt;

create table departement(
    DeptNum varchar(15) primary key not null default 'DeptNum'||nextval('sqdpt'),
    DName varchar(20),
    Loc varchar(20)
);
insert into departement (DName, Loc) values ('RESEARCH','DALLAS' );
insert into departement (DName, Loc) values ('SALES','CHICAGO' );
insert into departement (DName, Loc) values ( 'OPERATIONS','BOSTON');
select*from departement;

create table emp(
    EmpNum varchar(15) primary key not null default 'Emp'||nextval('sqemp'),
    Nom varchar(20),
    DeptNum varchar(20) references departement(DeptNum)
);

insert into emp (Nom , DeptNum) values ('Lars','DeptNum1' );
insert into emp (Nom , DeptNum) values ('Myranto','DeptNum2' );
insert into emp (Nom , DeptNum) values ( 'Mahatoky','DeptNum3');
select*from emp;

